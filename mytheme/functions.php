<?php

add_action('after_setup_theme', 'mytheme_setup');

function mytheme_setup() {

    load_theme_textdomain('mytheme');

    add_theme_support('title-tag');

    add_theme_support('custom-logo',[
        'height' => 40,
        'width' => 140,
        'flex-height' => true
    ]);

    add_theme_support('post_thumbnails');

    set_post_thumbnail_size(150,150);

    add_theme_support('menus');

    register_nav_menu('main', 'Main menu ');
}

//подключение стилей
add_action( 'wp_enqueue_scripts', 'mytheme_scripts' );

function mytheme_scripts() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', []);
    wp_enqueue_style( 'bootstrap-grid', get_template_directory_uri() . '/css/bootstrap-grid.min.css', []);
    wp_enqueue_style( 'bootstrap-reboot', get_template_directory_uri() . '/css/bootstrap-reboot.min.css', []);
    wp_enqueue_style('styles', get_stylesheet_uri() );

    wp_deregister_script('jquery');
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js');
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', [], false );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', ['jquery'], false );
}

// получает адресс картинки поста по id поста
function the_url_img($id_post) {
    $img = get_attached_media( 'image', $id_post );
    $img = array_shift($img);
    $img_url = $img->guid;

    echo $img_url;
}

// вывод количества просмотров постов
function get_post_views($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 просмотров";
    }
    return $count.' просмотров';
}

// изменение количества просмотров постов
function set_post_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//вывод имя и фамилии
function get_full_name() {
    $full_name = get_the_author_meta('user_firstname') . ' ' . get_the_author_meta('user_lastname');
    return $full_name;
}

//вывод популярных постов
function get_top_posts($posts) {
    $popular = new WP_Query('showposts='.$posts.'&meta_key=post_views_count&orderby=meta_value_num' );

    return $popular;
}

add_filter( 'get_the_excerpt', 'length_excerpt' );

//меняет длину цитаты на 100 символов
function length_excerpt($output) {
    global $post;
    $output = substr($output, 0, 100);
    $output =$output . '<a href="'. get_permalink($post) . '">Читать дальше...</a>';

    return $output;
}

add_action('wp_ajax_send_form', 'send_form');

//принимает и сохраняет емейл подписки
function send_form(){
    $email = $_POST['email'];

    if (is_email($email)) {
        global $wpdb;

        $table_name = $wpdb->prefix . "subscribers";

        if ($wpdb->get_results("SHOW TABLES LIKE '$table_name'") != $table_name) {

            $sql = "CREATE TABLE " . $table_name . " ( sub_id INT(9) UNSIGNED AUTO_INCREMENT PRIMARY KEY, sub_email VARCHAR(50) NOT NULL)";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);

            $wpdb->insert(
                $table_name,
                ['sub_email' => $email],
                ['%s']
            );
        } else {
            $wpdb->insert(
                $table_name,
                ['sub_email' => $email],
                ['%s']
            );
        }
    }
}

add_action( 'customize_register', 'customize_register' );

//изменение строки в футоре из админки
function customize_register( $wp_customize ) {

    $wp_customize->add_setting(
        'footer_copy' ,
        [
        'default'     => esc_html__('Copyright text', 'mytheme'),
        'transport'   => 'refresh',
        ]
    );

    $wp_customize->add_section(
        'footer_setting' ,
        [
        'title'      => esc_html__( 'Footer settings', 'mytheme' ),
        'priority'   => 121,
        ]
    );

    $wp_customize->add_control(
        'footer_copy',
        [
            'label'    => esc_html__( 'Footer settings', 'mytheme' ),
            'section'  => 'footer_setting',
            'settings' => 'footer_copy',
            'type'     => 'textarea',
        ]
    );
}







