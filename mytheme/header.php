<!doctype html>
<html lang="en">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php wp_head(); ?>
</head>


<body <?php body_class();?>
    <div id="page" class="content">
        <header class="header" id="header">
            <div class="container ">
                <div class="header_wrap row justify-content-between ">
                    <div class="col-2">
                        <a href="<?php echo home_url()?>">
                           <?php the_custom_logo();?>
                        </a>
                    </div>

                    <div class="col-5">
                        <nav class="header-menu" id="menu">
                            <?php
                            wp_nav_menu([
                                'theme_location' => 'main',
                                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s<ul/>',
                                'menu_class' => '',
                                'menu_id' => '',
                                'depth' => '2'
                            ])?>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
