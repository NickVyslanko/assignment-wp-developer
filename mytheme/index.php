<?php get_header(); ?>

<div class="content" >
    <div class="container">
        <h3 class="text-center title-last-posts">Последние посты</h3>
        <div class="row">

            <?php
            $posts = wp_get_recent_posts([
                'numberposts' => 3,
                'orderby'     => 'post_date',
                'order'       => 'DESC',
                'post_status' => 'publish',
            ]);

            foreach( $posts as $p ){
                setup_postdata( $p );
            ?>

            <div class="col-md-4 col-md-4">
                <article class="blog_post">
                    <h4 class="blog_post_title text-left">
                        <a href="<?php the_permalink($p['ID']); ?>">
                            <?php echo $p['post_title'];?>
                        </a>
                    </h4>

                    <div class="blog_post_img">
                        <a href="<?php the_permalink($p['ID']); ?>">
                            <img class="img_post" src="<?php  the_url_img($p['ID']); ?>" alt="img">
                        </a>
                    </div>
                    <div class="blog_post_data">
                        <div class="row text-center">
                            <div class="col text-left ">
                                <?php echo mysql2date('j F Y', $p['post_date'])?>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col text-left">
                                Автор:<?php echo get_full_name();?>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-6 text-left">
                            <?php echo get_post_views($p['ID']); ?>
                        </div>
                        <div class="col-md-6">
                            <a href="<?php the_permalink($p['ID']); ?>">
                                Посмотреть пост
                            </a>
                        </div>
                    </div>
                </article>
            </div>
            <?php
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>

    <div class="populargb">
        <div class="container">
            <h3 class="text-center title-top-posts">Популярные посты</h3>
            <div class="row">

                <?php $populargb = get_top_posts(3);
                    while ( $populargb->have_posts() ) {
                    $populargb->the_post();
                ?>

                <div class="col-4">
                    <div class="row ">
                        <div class="col text-center">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-left">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-left">
                            <?php echo get_post_views(get_the_ID()); ?>
                        </div>
                    </div>
                </div>

                <?php
                }
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>

    <div class="container ">
        <div class="row">
            <div class="col-12">
                <form id="contact" action="<?php echo admin_url('admin-ajax.php?action=send_form') ?>" method="post">
                    <h3 class="text-center title-subscriptions">Подписка</h3>
                    <div class="form-row justify-content-center">
                        <div class="col-6 ">
                            <label for="email">Введите ваш емеил</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="E-mail" required>
                        </div>
                    </div>
                    <div class="form-row justify-content-center">
                        <div>
                            <input  class="form-submit btn btn-outline-primary" value="Отправить" type="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>