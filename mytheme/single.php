<?php

get_header(); ?>

<?php set_post_views(get_the_ID()); ?>

    <div class="content container wrap">
        <main id="main" class="site-main" role="main">

            <?php
            // Start the Loop.
            while ( have_posts() ) :
                the_post();
            ?>

            <article class="blog_post">
                <h1 class="blog_post_title text-center">
                        <?php the_title();?>
                </h1>
                <div class="row test-center justify-content-center">
                    <?php
                    the_content();?>
                </div>


                <div class="blog_post_data">
                    <div class="row text-right">
                        <div class="col">
                            Автор:<?php echo get_full_name();?>
                        </div>
                    </div>
                    <div class="row text-right">
                        <div class="col">
                            <?php echo mysql2date('j F Y',the_date())?>
                        </div>
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-12">
                        <?php echo get_post_views(get_the_ID()); ?>
                    </div>
                </div>
            </article>



                <?php
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End the loop.
            ?>

        </main>
    </div>

<?php get_footer();?>
