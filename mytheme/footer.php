
    <footer class="page-footer font-small footer">
        <div class="container">
            <div class="footer-copyright text-center"><?php echo get_theme_mod('footer_copy'); ?></div>
            <div class="footer-copyright text-center">
                <a href="https://woordpress.org/"> Wordpress.org</a>
            </div>
        </div>
    </footer>

</body>
    <?php wp_footer() ?>
</html>