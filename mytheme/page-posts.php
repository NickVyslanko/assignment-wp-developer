<?php get_header(); ?>

<div class="container ">
    <div class="col text-center">
        <?php
        $wp_query = new WP_Query(array(
            "post_type" => "post",
            "post_status" => "publish"
        ));

        if($wp_query->have_posts()) {

            while($wp_query->have_posts()) {
                $wp_query->the_post();?>

                <article class="blog_post">
                    <div class="wrap_post">
                        <h4 class="blog_post_title text-center">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title();?>
                            </a>
                        </h4>
                        <div class="posts_img text-center">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img_posts" src="<?php the_url_img(get_the_ID()); ?>" alt="img">
                            </a>
                        </div>
                        <div class="row">
                            <div class="col text-center">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                        <div class="blog_post_data">
                            <div class="row text-center">
                                <div class="col text-center ">
                                    <?php echo mysql2date('j F Y', the_date())?>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col text-center">
                                    Автор:<?php echo get_full_name();?>
                                </div>
                            </div>
                            <div class="col text-center">
                                <?php echo get_post_views(get_the_ID()); ?>
                            </div>
                        </div>
                    </div>
                </article>
            <?php
            }
            wp_reset_postdata();
        ?>
    </div>
    <?php
    }
    else {
        echo "<h2>Записей нет.</h2>";
    }

?>
</div>
<?php get_footer(); ?>